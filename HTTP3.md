HTTP/3 là gì ?, và tại sao nó lại quan trọng ?
=======================================
HTTP over QUIC, hay còn gọi là HTTP/3 mang lại nhiều tính năng và hiệu suất cho giao thức HTTP

-----------------------------------------------------------------------


![](https://miro.medium.com/max/8064/0*oudyG8yVAEkJ7vm5)

Ảnh minh họa bởi [JJ Ying](https://unsplash.com/@jjying?utm_source=medium&utm_medium=referral)
trên [Unsplash](https://unsplash.com?utm_source=medium&utm_medium=referral)

Khi nghiên cứu về internet và các công nghệ đằng sau nó, bạn có thể đã bắt gặp thuật ngữ này: **HTTP**. HTTP là giao
thức truyền siêu văn bản, là cốt lõi của web và là giao thức phổ biến để truyền dữ liệu văn bản. Bạn chắc chắn đã sử
dụng nó, vì hầu hết các trang web bạn truy cập đều sử dụng giao thức HTTP
<iframe
  src="https://codepen.io/team/codepen/embed/preview/PNaGbb"
  style="width:100%; height:300px;"
></iframe>

# Giới thiệu

Tóm lược về lịch sử phát triển của HTTP.
-----------------------

Phiên bản đầu tiên được giới thiệu là phiên bản HTTP/0.9 . Tim Berners-Lee là người tạo ra nó vào năm 1989, và nó đã
được đặt tên thành HTTP/0.9 vào năm 1991. HTTP/0.9 bị giới hạn và nó chỉ có thể làm những thứ cơ bản. Nó không thể trả
về mọi thứ ngoại trừ trang web, không hỗ trợ cookies và các tính năng mới. vào năm 1996 , HTTP/1.0 được phát hành, mang
tới các tính năng mới như POST request và khả năng gửi một số thứ ngoại trừ việc chỉ trả lại trang web . Tuy nhiên, nó
vẫn còn là một chặng đường dài so với những gì ngày hôm nay. HTTP/1.1 được phát hành vào 1997 và đã được sửa đổi 2 lần,
lần thứ nhất vào năm 1999 và lần thứ 2 vào năm 2007. Nó mang lại nhiều tính năng mới quan trọng như cookies và thiết lập
kết nối liên tục. Cuối cùng, vào năm 2015, HTTP/2 đã được phát hành và cho phép tăng hiệu suất, làm cho những thứ như
event được gửi từ máy chủ và khả năng gửi nhiều yêu cầu cùng một lúc. HTTP/2 vẫn còn mới và chỉ được sử dụng chưa tới
1/2 của tất cả các trang web hiện nay..

HTTP/3: Phiên bản mới nhất của HTTP
----------------------------------

HTTP/3, hay HTTP over QUIC, thay đổi HTTP rất nhiều. HTTP truyền thống được thực hiện qua TCP(Transmission Control
Protocol),là giao thức truyền tải hướng kết nối.Tuy nhiên, TCP được phát triển vào năm 1974, vào thời kỳ đầu của
internet. Khi TCP ban đầu được tạo ra, các tác giả của nó không thể dự đoán được sự phát triển của web. Do TCP đã lỗi
thời nên TCP đã hạn chế HTTP trong một thời gian cả về tốc độ và bảo mật. Bây giờ, vì HTTP/3, HTTP không bị giới hạn
nữa. Thay vì TCP, HTTP/3 sử dụng một giao thức mới do Google phát triển vào năm 2012, được gọi là QUIC (phát âm là "
quick",có nghĩa là nhanh chóng). Điều này giới thiệu nhiều tính năng mới cho HTTP.

Đặc trưng
========

Yêu cầu ghép kênh nhanh hơn (Faster Request Multiplexing).
---------------------------

![](https://miro.medium.com/max/12000/0*Oz9x1jnI9c2V5qmd)
Ảnh minh họa

Trước HTTP/2, các trình duyệt chỉ có thể gửi một request tới máy chủ tại một thời điểm. Điều này khiến trang web tải
chậm vì trình duyệt chỉ tải được một nội dung tại một thời điểm. HTTP / 2 đã giới thiệu khả năng tải nhiều nội dung cùng
một lúc, nhưng TCP không được tạo ra cho việc này. Nếu một trong các yêu cầu không thành công, TCP sẽ yêu cầu trình
duyệt thực hiện lại tất cả các yêu cầu. Vì giao thức TCP đã bị loại bỏ tại HTTP/3 và thay thế bằng QUIC, HTTP/3 đã giải
quyết được vấn đề này. Với HTTP/3, trình duyệt chỉ cần thực hiện lại yêu cầu không thành công. Do đó, HTTP/3 nhanh
hơn và đáng tin cậy hơn.


Mã hóa nhanh hơn
-----------------

![](https://miro.medium.com/max/8064/0*YCjpKNI1WGsHrXla)
Ảnh minh họa

HTTP/3 tối ưu hóa tiến trình "bắt tay" cho phép các yêu cầu HTTP của trình duyệt được mã hóa. QUIC kết hợp kết nối ban
đầu với "bắt tay" TLS, làm cho nó an toàn theo mặc định và nhanh hơn.

Triển khai
==============

Tiêu chuẩn hóa
---------------

Tại thời điểm viết bài này, HTTP/3 và QUIC chưa được chuẩn hóa. Nhóm công tác IETF hiện đang làm việc trên bản draft để
tiêu chuẩn hóa QUIC. Phiên bản QUIC cho HTTP/3 được sửa đổi một chút, sử dụng TLS thay vì mã hóa của Google, nhưng nó có
cùng ưu điểm.

Trình duyệt hỗ trợ
---------------

Hiện tại, Chrome hỗ trợ HTTP/3 theo mặc định do Google tạo ra giao thức QUIC và đề xuất cho HTTP over QUIC. Firefox cũng hỗ trợ giao thức trong các phiên bản 88+. Safari 14 hỗ trợ HTTP/3, nhưng chỉ khi cờ tính năng thử
nghiệm được bật.

![](https://miro.medium.com/max/2740/1*DwY-vtr6Qzj2TdbW4KaTAw.png)
Các trình duyệt hỗ trợ HTTP/3 (
Nguồn: [Can I Use](https://caniuse.com/http3))

Hỗ trợ Serverless/CDN
----------------------

Cho đến nay, chỉ một số máy chủ hỗ trợ HTTP/3, nhưng thị phần của chúng đang tăng lên. Cloudflare là một trong những
công ty đầu tiên Google hỗ trợ HTTP/3, vì vậy chức năng Serverless và CDN của họ đều tuân thủ HTTP/3. Ngoài ra,
Google Cloud và Fastly tuân thủ HTTP/3. Thật không may, Microsoft Azure CDN và AWS CloudFront dường như không hỗ trợ
HTTP/3 vào thời điểm hiện tại. Nếu bạn muốn dùng thử HTTP/3, [QUIC.Cloud](https://quic.cloud/) là một cách thú vị (
mặc dù thử nghiệm) để thiết lập một CDN HTTP/3 trong bộ nhớ đệm phía trước máy chủ của bạn. Cloudflare, Fastly và
Google Cloud cũng hỗ trợ tốt HTTP/3 và "production-ready" hơn.

Tổng kết
==========

HTTP/3 vẫn là một bản cập nhật thử nghiệm cho HTTP và nó có thể sẽ thay đổi. Tuy nhiên, hơn một nửa số người dùng ủng
hộ hình thức HTTP/3 hiện tại. Nếu bạn chuẩn bị cập nhật cho việc triển khai của mình, thì đó có thể là một bước tăng
hiệu suất đáng hoan nghênh. Hy vọng bạn thấy thú vị khi đọc và học được điều gì đó từ bài viết này.

Nguồn tài liệu
=========

* [Usage Statistics of HTTP/2 for Websites, May 2021 (w3techs.com)](https://w3techs.com/technologies/details/ce-http2)
* [QUIC Working Group (quicwg.org)](https://quicwg.org/)
* [Can I use… Support tables for HTML5, CSS3, etc](https://caniuse.com/http3)
* [Homepage — QUIC.cloud](https://quic.cloud/)

_Nhiều nội dung hơn tại_ [**_plainenglish.io_**](http://plainenglish.io)
